﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;

public class LobbyManager : NetworkLobbyManager
{
    public GameObject Lobby;


    private void Start()
    {
        Lobby.SetActive(false);
    }

    public override void OnStartHost()
    {
        base.OnStartHost();
        print("a game has created");
        Lobby.SetActive(true);
    }

    internal NetworkMatch.DataResponseDelegate<MatchInfo> OnMatchJoined()
    {
        throw new NotImplementedException();
    }
}
