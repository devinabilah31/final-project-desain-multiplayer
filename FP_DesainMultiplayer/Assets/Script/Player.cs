﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Player : NetworkBehaviour
//{
//    public float moveSpeed = 5f;

//    private void Start()
//    {
        
//    }

//    private void Update()
//    {
//        Jump();
//        Vector3 movement = new Vector3(Input.GetAxis("Horizontal"), 0f, 0f);
//        transform.position += movement * Time.deltaTime * moveSpeed;
//    }

//    void Jump()
//    {
//        if (Input.GetButtonDown("Jump"))
//        {
//            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, 5f), ForceMode2D.Impulse);
//        }
//    }
//}

{
    [SerializeField] private LayerMask PlatformLayerMask;   
    public float speed;
    Rigidbody2D rb;
    BoxCollider2D bc;
    public static float healthAmount;
    


// Start is called before the first frame update
void Start()
{
    rb = GetComponent<Rigidbody2D>();
    bc = GetComponent<BoxCollider2D>();
    healthAmount = 1;

       transform.position += new Vector3(
       Random.Range(-1f, 1f),
       Random.Range(-1f, 1f),
       0f);

}

// Update is called once per frame
private void Update()
{
        if (!isLocalPlayer)
        {
            return;
        }

        if (IsGrounded() && Input.GetKeyDown(KeyCode.Space))
        {
            float jumpVelocity = 8f;
            rb.velocity = Vector2.up * jumpVelocity;
        }

        if (healthAmount <= 0)
        Destroy(gameObject);
}

void FixedUpdate()
{
    float move = Input.GetAxis("Horizontal");
    rb.velocity = new Vector2(speed * move, rb.velocity.y);

}

void OnTriggerEnter2D(Collider2D collision)
{
    if (collision.gameObject.name.Equals("Spike"))
        healthAmount -= 0.1f;
}
private bool IsGrounded()
{
    RaycastHit2D raycastHit2d = Physics2D.BoxCast(bc.bounds.center, bc.bounds.size, 0f, Vector2.down, .1f, PlatformLayerMask);
    Debug.Log(raycastHit2d.collider);
    return raycastHit2d.collider != null;
}
}
