﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking.Match;
using UnityEngine.UI;

public class HostSetup : MonoBehaviour
{
    MatchInfoSnapshot match;
    public Text HostName;

    LobbyManager lobbymanager;
    GameObject LobbyParent;

    // Start is called before the first frame update
    void Start()
    {
        lobbymanager = GameObject.FindGameObjectWithTag("LMANAGER").GetComponent<LobbyManager>();
        LobbyParent = GameObject.FindGameObjectWithTag("LOBBYPARENT");
    }

    // Update is called once per frame
    public void Setup(MatchInfoSnapshot _match)
    {
        match = _match;
        HostName.text = match.name;

    }

    public void Join()
    {
        if (lobbymanager == null)
        {
            lobbymanager = GameObject.FindGameObjectWithTag("LMANAGER").GetComponent<LobbyManager>();
        }

        var go = LobbyParent.GetComponentsInChildren<Transform>(true);
        foreach(var item in go)
        {
            item.gameObject.SetActive(true);
        }
        
        lobbymanager.matchMaker.JoinMatch(match.networkId, "", "", "", 0, 0, lobbymanager.OnMatchJoined);
    }
}
