﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking.Match;

public class JoinRoom : MonoBehaviour
{
    LobbyManager lobbymanager;
    public GameObject Prefabforhost;
    public GameObject ParentforHost;

    // Start is called before the first frame update
    void Start()
    {
        lobbymanager = GameObject.FindGameObjectWithTag("LMANAGER").GetComponent<LobbyManager>();
        
    }

    public void RefreshList()
    {
        if(lobbymanager == null)
        {
            lobbymanager = GameObject.FindGameObjectWithTag("LMANAGER").GetComponent<LobbyManager>();
        } if(lobbymanager.matchMaker == null)
        {
            lobbymanager.StartMatchMaker();
        }
        lobbymanager.matchMaker.ListMatches(0, 20, "", true, 0, 0, onMatchList);
    }

    private void onMatchList(bool success, string extendedInfo, List<MatchInfoSnapshot> matchList)
    {
        if (!success)
        {
            print("please refresh");
        }

        foreach(MatchInfoSnapshot match in matchList)
        {
            GameObject ListGO = Instantiate(Prefabforhost);
            ListGO.transform.SetParent(ParentforHost.transform);
            HostSetup hostSetup = ListGO.GetComponent<HostSetup>();
            hostSetup.Setup(match);

        }
    }
}
